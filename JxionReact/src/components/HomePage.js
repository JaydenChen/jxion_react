import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Content, Card, CardItem, Text, Left, Body, Right } from 'native-base';

class HomePage extends Component {
  render() {
    return (
      <Container>
      <Left>
        <Container>
          <Content>
            <Card>
              <CardItem>
                <Left>
                  <Body>
                    <Text>NativeBase</Text>
                    <Text note>GeekyAnts</Text>
                  </Body>
                </Left>
              </CardItem>
              <CardItem cardBody>
                <Image
                source={require('../assets/cash_register.png')}
                style={{ height: 50, width: null, flex: 1, resizeMode: 'contain' }}
                />
              </CardItem>
            </Card>
          </Content>
        </Container>
      </Left>
      <Right>
        <Container>
          <Content>
            <Card>
              <CardItem>
                <Left>
                  <Body>
                    <Text>NativeBase</Text>
                    <Text note>GeekyAnts</Text>
                  </Body>
                </Left>
              </CardItem>
              <CardItem cardBody>
                <Image
                source={require('../assets/cash_register.png')}
                style={{ height: 50, width: null, flex: 1, resizeMode: 'contain' }}
                />
              </CardItem>
            </Card>
          </Content>
        </Container>
      </Right>
      </Container>
    );
  }
}

export default HomePage;
