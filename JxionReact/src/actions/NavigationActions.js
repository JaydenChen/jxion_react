/* eslint-disable */
import NAVIGATE from './types';

export const navigateTo = ({ prop, value }) => {
  return {
    type: NAVIGATE,
    payload: { prop, value }
  };
};
