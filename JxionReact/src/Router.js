import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import HomePage from './components/HomePage';
import Register from './components/Register';
const RouterComponent = () => {
  return (
    <Router sceneStyle={{ paddingTop: 65 }}>
      <Scene key="home" component={HomePage} title="Home Page"/>
      <Scene key="register" component={Register} title="Register"/>
    </Router>
  )
}
